#!/bin/bash

cp init.vim ~/.config/nvim/init.vim
cp plugins.vim ~/.config/nvim/plugins.vim

vim +PluginInstall +qall
