#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Use nvim instead of vim
if type nvim > /dev/null 2>&1; then
  alias vim='nvim'
fi

# Customize to your needs...

## ALIASES
alias tmux="TERM=screen-256color-bce tmux"

# Alias for tensorflow environment
alias tfon="source venv/bin/activate"
alias tfoff="deactivate"

## Fix ZSH and Rake Parameters
alias rake='noglob rake'
unsetopt nomatch

## GPG
# Add the following to your shell init to set up gpg-agent automatically for every shell
if [ -f ~/.gnupg/.gpg-agent-info ] && [ -n "$(pgrep gpg-agent)" ]; then
    source ~/.gnupg/.gpg-agent-info
    export GPG_AGENT_INFO
else
    eval $(gpg-agent --daemon)
fi

export GPG_TTY=$(tty)

# Rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# SSH agent workaround
# eval "$(ssh-agent -s)"
# ssh-add -K ~/.ssh/id_rsa
{ eval `ssh-agent`; ssh-add -A; } &>/dev/null

# Direnv
eval "$(direnv hook zsh tmux)"

export PATH="$HOME/.bin:$PATH"
eval "$(rbenv init - --no-rehash)"
export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"

# Rust
source $HOME/.cargo/env

# Golang
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# Python3
export PATH=/usr/local/opt/python/libexec/bin:$PATH
export PATH=/Users/mtanzi/Library/Python/3.7/bin:$PATH

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/slss.zsh ]] && . /Users/mtanzi/Projects/serverless/aws-node-simple-http-endpoint/node_modules/tabtab/.completions/slss.zsh
